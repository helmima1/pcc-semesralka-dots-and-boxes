# Hra Dots and Boxes

###### Autor: Martin Helmich

## Zadání
Implementujte hru Dots and Boxes ve dvou módech - multiplayer (pro 2 hráče) a singleplayer (proti počítači). Při každém  tahu hráč zadává souřadnice čtverce a specifikuje stranu, kterou chce označit. Program automaticky vyhodnocuje hráčovy pohyby a vykresluje je na aktualizovanou hrací plochu. V multiplayer verzi se hráči střídají po tazích, kdežto v singleplayer se hráč střídá s počítačem.

## Popis hry dots and boxes
Hra dots and boxes je jednoduchá hra, ze začátku ovšem nebývá úplně intuitivní. Pojďme si jí vysvětlit na příkladech.

### Provádění tahů
Na začátku se hráči zobrazí hrací pole, které se skládá ze čtvrců v mřížce X Y. Věnujme teď pozornost stranám, které tvoří jednotlivé čtverce.

```
     0    1    2    y
   ⋅ -- ⋅ -- ⋅ -- ⋅
0  |    |    |    |   
   ⋅ -- ⋅ -- ⋅ -- ⋅
1  |    |    |    |   
   ⋅ -- ⋅ -- ⋅ -- ⋅
x
```

Tahy v této hře spočívají v tom, že označujeme strany jednotlivých čtverců. Zvolme například horní stranu čtverce o součadnicích 0 0 (v posloupnosti x y).

```
     0    1    2    y
   ⋅(--)⋅ -- ⋅ -- ⋅
0  |    |    |    |   
   ⋅ -- ⋅ -- ⋅ -- ⋅
1  |    |    |    |   
   ⋅ -- ⋅ -- ⋅ -- ⋅
x
```
Ukončili jsme tah a hraje protihráč. Ten též vybere stranu stejným způsobem. Řekněme, že vybere levou stranu čtverce 0 0.

```
     0    1    2    y
   ⋅(--)⋅ -- ⋅ -- ⋅
0 (|)   |    |    |   
   ⋅ -- ⋅ -- ⋅ -- ⋅
1  |    |    |    |   
   ⋅ -- ⋅ -- ⋅ -- ⋅
x
```
Stana je vybrané a jsme zase na řadě my. Hráči se takto střídají do té doby, dokud nejsou všechny strany v hracím poli označené.

### Získání bodu
Bod získává hráč ve chvíli, co svým tahem uzavře čtverec, tj. označí jeho čtvrtou stranu. Řekněme, že máme hrací pole v následujícím stavu:
```
     0    1    2    y
   ⋅(--)⋅ -- ⋅(--)⋅
0 (|)  (|)   |    |   
   ⋅ -- ⋅ -- ⋅ -- ⋅
1  |    |   (|)  (|)   
   ⋅ -- ⋅ -- ⋅ -- ⋅
x
---------- Curent score : ----------
Player 1: 0 points
Player 2: 0 points
------------------------------------
```
Následně označme dolní stranu čtverce o souřadnicích 0 0. V takovém případě uzavíráme čtverec a získáváme bod.
```
     0    1    2    y
   ⋅(--)⋅ -- ⋅(--)⋅
0 (|)11(|)   |    |   
   ⋅(--)⋅ -- ⋅ -- ⋅
1  |    |   (|)  (|)   
   ⋅ -- ⋅ -- ⋅ -- ⋅
x
---------- Curent score : ----------
Player 1: 1 points
Player 2: 0 points
------------------------------------
```
V případě, že získáme v našem tahu bod, hrajeme ještě jednou. Získané pole je v dokumentaci označené jedničkami, v  implementaci hry je vyznačeno hráčovou barvou.
Vítězí hráč s vyšším počtem získaných bodů na konci hry, tj. ve chvíli, co jsou již všechny strany označené.

Příkladem implementace na internetu může být například tato stránka: [Gametable - Dots and Boxes](https://gametable.org/games/dots-and-boxes/).

## Podporované funkcionality

- Multithreading
  - Vykreslovací vlánko (outputThread)
  - Početní vlákno (computeThread)
  - Vlákno zpracovávající vstup (outputThread)
- Zpracování vstupu v rámci terminálu
- Ukončení programu pomocí stisknutí klávesy `q` na vstupu
- Zobrazení instrukcí po vložení vstupu `--help`
- Výpis chybových hlášek, validace vstupu
- 2 režimy hry:
  - Hráč proti hráči
  - Hráč proti počítači

## Kompilace programu
Program lze zkompilovat následujícím příkazem:
```bash
cmake -Bcmake-build-debug -H. -DCMAKE_BUILD_TYPE=Debug
cmake --build cmake-build-debug
```

## Spuštění a argumenty
Hra lze spustit bez argumentů, nebo s argumenty. Bez argumentů se inicializuje pole o velikosti 2x3 v módu pro 2 hráče.

Pokud uživatel zadá argumenty, tak musí specifikovat velikost pole a herní mód. 
- Velikost pole se specifikuje počtem řádků (-rows) a počtem sloupců (-cols). Lze zadat pouze kladné nenulové hodnoty. Počet řádků a sloupců zároveň musí být menší nebo roven 50ti kvůli správnému vykreslování mřížky (v takovýchto rozměrech je hra ovšem již uživatelsky nepříjemná).
- Pro mód Multiplayer zadává uživatel  "-pvp" (Player Vs Player) a pro Single player zadává "-pvc" (Player Vs Computer) jako poslední argument.

Pokud zadáte argumenty špatně, tak se hra nespustí a vypíše chybovou hlášku.

Příklady spuštění programu:
```
./pcc_semesralka_dots_and_boxes
./pcc_semesralka_dots_and_boxes -rows 2 -cols 3 -pvp
./pcc_semesralka_dots_and_boxes -rows 4 -cols 5 -pvc
./pcc_semesralka_dots_and_boxes -rows 20 -cols 18 -pvc
```



## Ovládání programu
Po zobrazení hracího pole a instrukcí je hráč vyzván k zadání pohybu. Hráč zároveň může zadat `q` pro ukončení programu nebo `--help` pro zobrazení instrukcí.

### Zadávání tahů
Tah zadáváme ve formě specifikace souřadnic čtverce a identifikace jeho strany. Strana je definovaná pomocí vložení znaků `-l` pro levou stranu (left), `-r` pro pravou stranu (right), `-u` pro horní stranu (up) a `-d` pro dolní stranu (down)
Příkladem mohou být následující tahy:
```
0 0 -u
1 1 -l
0 1 -r
1 0 -l
```
Z toho plyne, že 2 vstupy mohou mít stejný výstup, jelikož pravá strana jednoho čtverce může být stejná jako levá strana jeho souseda. Příkladem stejného výstupu mohou být následující 2 vstupy:
```
0 0 -r
0 1 -l
```

### Ukončení programu
Program je ukončen automaticky, pokud dojde k označení všech stran v hracím poli. Program je možné kdykoliv ukončit klávesou `q` a stisknutím enter.

### Nápověda
Hráč si může zobrazit nápovědu pomocí vložení vstupu `--help` a stisknutí klávesy enter. V takovémto případě se hráči vypíše následujcící nápověda:

```
--------------------- INSTRUCTIONS --------------------------------
Goal of this game is to mark lines around as many sqares as possible.
You can create a line by first specyfing coordinates of its boardering square and second sepcifying the sqare's side.
You can specify the side by entering -l for left, -r for right, -u for up or -d for down. Example entry can be "0 1 -u".
That means there can be up to 2 ways of marking one specific line.
You recieve a point when you make the 4th line around a square in your move, therefore closing it in it's boundaries
The player with the bigger amounth of points wins the game.
To show help enter "--help" and to quit game enter "q".
```


## Stručný popis implementace
Kód je patřičně okomentován, účely jednotlivých funkcí a proměnných jsou uvedené v hlavičkovém souboru `game.h`. Implementace je vytvořená v rámci jedné třídy `Game`. Testovací scénáře jsou v separátním souboru `TEST_SCENARIOS.md`.

### Vlákna
Hra používá pro implementaci 3 vlákna: ```Game::inputThread```, ```Game::computeThread``` a ```Game::outputThread```. Vlákna se spouští ve funkci ```Game::start()```.

Po inicializaci a spuštění hry se vykreslí hrací pole a uživatel může zadávat vstup. V rámci výpočetního vlákna se po každém zadaném vstupu kontroluje, zda nebyl vstup zadaný chybně, v takovémto případě vyšle signál ```Game::outputThread``` pro vykreslení hlášky a žádný výpočet se neprovádí.
Pokud byl vstup zadán správně, tak je tah evaluován pomocí funkce ```Game::markSpotAndEvaluateWin()```. V případě, že jsou již všechny čtverce zaplněné, ukončí vlákno ```Game::computeThread``` svůj běh, čímž vyšle signál na ukončení i ostatním vláknům.

### Souřadnicový systém
Implementace používá 2 různé notace pro zadávání souřadnic z důvodu většího uživatelského pohodlí. V proměnné `Game::board` jsou souřadnice uložené vzhledem k souřadnicím jednotlivých stran. To znamená, že na lichých řádcích jsou uložené horní a dolní strany a na sudých řádcích pouze levé a pravé strany. Souřadnice čtverců jsou uchovávané v proměnné `Game::wonSpots`, kde je zároveň uložená informace o tom, zda byl čtverec již získaný některým z hráčů. Převod souřadnic čtverců na souřadnice stran je prováděn ve funkci `Game::convert_box_to_coord(size_t row, size_t col, string option)`. Porovnání souřadnic stran a čtverců je vidět na následujících vizualizacích.

Souřadnice čtverců (stejné jak vidí uživatel):
```
     0    1    2    y
   ⋅ -- ⋅ -- ⋅ -- ⋅
0  |    |    |    |   
   ⋅ -- ⋅ -- ⋅ -- ⋅
1  |    |    |    |   
   ⋅ -- ⋅ -- ⋅ -- ⋅
x
```
Souřadnice stran:
```
   0    1    2    3 
0  ⋅ -- ⋅ -- ⋅ -- ⋅
1  |    |    |    |   
2  ⋅ -- ⋅ -- ⋅ -- ⋅
3  |    |    |    |   
4  ⋅ -- ⋅ -- ⋅ -- ⋅
x
```
Z toho plyne, že každý sudý řádek má v souřadnicovém systému stran o jeden prvek více, jelikož pravých a levých stran je o 1 více než horních a dolních. Například pravá strana čtverce o souřadnicích 0 2 má souřadnice 1 3 a horní strana toho samého čtverce má souřadnice 0 2.

### Režimy chodu
Pro informaci o režimu chodu je využitá proměnná `Game::currentGameMode`. Pro tyto učely je vytvořený nový enumeration `Game::GameMode`, který obsahuje zatím 2 možné hodnoty - `PlayerVsPlayer` a `PlayerVsSimpleComputer`. Momentální verze počítačového protihráče je implementována tak, že dává pouze náhodné tahy na místa, která ještě nejsou označena (proto "simple computer"). V případě budoucího vývoje stačí přidat do enum další herní mód a vytvořit patřičnou funkci, která implementuje logiku vytvářených tahů. 



## Vize budoucího vývoje
V momentální implementaci je protihráč implementován jednoduše tím, že označuje náhodné dosud neoznačené strany. Do budoucna by šlo naimplementovat lepší verzi AI, která by volila pole podle přednastavené strategie.

