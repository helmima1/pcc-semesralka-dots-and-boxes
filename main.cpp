#include <iostream>
#include "src/game.h"

int main(int argc, char* argv[]) {
    int rows = 0, cols = 0;
    std::string gameMode;

    // Parse arguments
    for (int i = 1; i < argc; ++i) {
        std::string arg = argv[i];
        if (arg == "-rows" && i + 1 < argc) {
            rows = std::atoi(argv[++i]);
        } else if (arg == "-cols" && i + 1 < argc) {
            cols = std::atoi(argv[++i]);
        } else if (arg == "-pvp") {
            gameMode = "PvP";
        } else if (arg == "-pvc") {
            gameMode = "PvC";
        } else {
            std::cerr << "Invalid arguments. Try it again with the correct ones specified in the documentation."<< std::endl;
            return 1;
        }
    }

    // Validate arguments
    if (((rows > 50 || cols > 50) || (rows <= 0 || cols <= 0)) && !gameMode.empty()) {
        std::cerr << "Invalid arguments. Try it again with the correct ones specified in the documentation."<< std::endl;
        return 1;
    }

    if (gameMode.empty() && (rows > 0 || cols > 0)) {
        std::cerr << "Invalid arguments. Try it again with the correct ones specified in the documentation."<< std::endl;
        return 1;
    }

    // Game logic
    if (argc == 1) {
        // No arguments - default game
        Game g;
        g.start();
    } else if (rows > 0 && cols > 0 && !gameMode.empty()) {
        // Arguments provided - customized game
        if (gameMode == "PvP") {
            Game g(rows, cols, Game::PlayerVsPlayer);
            g.start();
        } else if (gameMode == "PvC") {
            Game g(rows, cols, Game::PlayerVsSimpleComputer);
            g.start();
        }
    } else {
        std::cerr << "Invalid arguments. Try it again with the correct ones specified in the documentation."<< std::endl;
    }

    return 0;
}
