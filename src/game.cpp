#include <iostream>
#include <sstream>
#include <thread>
#include "game.h"
#include <random>

using namespace std;

#define ANSI_CLEAR  "\x1B[2J\x1B[H"
#define ANSI_COLOR_RESET "\x1B[m"
#define COLOR_RED   "\x1B[91m"
#define COLOR_GREEN "\x1B[92m"

#define BG_COLOR_GREEN      "\x1B[42m"
#define BG_COLOR_RED        "\033[41m"
#define BG_COLOR_DEFAULT    "\033[0m"

void Game::createPlayingBoard() {
    // Example: For playing board of size 2x3 (xRows x yCols), 5x4 board is required
    // Formula is following: boardRows = 2 x xRows + 1; boardCols = yCols + 1
    boardRows = xRows * 2 + 1;
    boardCols = yCols + 1;
    for (int i = 0; i < boardRows; ++i) {
        if (i % 2 == 0) {
            board.push_back(vector<int>(boardCols - 1, 0));
        } else {
            board.push_back(vector<int>(boardCols, 0));
        }
    }
    // Iniciace wonSpots
    for (int i = 0; i < xRows; ++i) {
        wonSpots.push_back(vector<int>(yCols, 0));
    }
    squares_left = xRows * yCols;
    if (currentGameMode != Game::PlayerVsPlayer) {
        // add all coordinates to vector, that are in the board (therefore free at the start of the game)
        // if the field is odd, don't add last item
        for (int row = 0; row < boardRows; ++row) {
            // on even rows there is one more item, since there are sides of the field
            if (row % 2 == 1) {
                for (int col = 0; col < boardCols; ++col) {
                    freeSpaces.push_back(make_pair(row, col));
                }
            } else {
                for (int col = 0; col < boardCols - 1; ++col) {
                    freeSpaces.push_back(make_pair(row, col));
                }
            }

        }
    }
}

Game::Game(size_t rows, size_t cols, GameMode gameMode) : xRows(rows), yCols(cols), currentGameMode(gameMode) {
    createPlayingBoard();
}

Game::Game(GameMode gameMode) : currentGameMode(gameMode) {
    xRows = 2;
    yCols = 3;
    createPlayingBoard();
}

Game::Game() {
    currentGameMode = Game::PlayerVsPlayer;
    xRows = 2;
    yCols = 3;
    createPlayingBoard();
}

void Game::start() {
    thread input(&Game::inputThread, this);
    thread output(&Game::outputThread, this);
    thread compute(&Game::computeThread, this);

    input.join();
    output.join();
    compute.join();
}

void Game::inputThread() {
    cout << ANSI_CLEAR;
    unique_lock<mutex> lock(inputMutex);
    cout << "--------------------- WELCOME TO DOTS AND BOXES GAME! ---------------------" << endl;
    cout << "In front of you is a playing field of size " << xRows << " x "<< yCols << "." << endl;
    if (currentGameMode == Game::PlayerVsPlayer) {
        cout << "This version of the game is designed for 2 players." << endl;
    } else if (currentGameMode == Game::PlayerVsSimpleComputer) {
        cout << "In this version you play against a computer." << endl;
    }
    cout << "To quit the game, ender \"q\"" << endl;
    cout << "Are you new here? Enter \"--help\" to show instructions." << endl;
    printHelp();

    while(!quitRequested) {
        this_thread::sleep_for(chrono::milliseconds(200));
        if(getUserInput()) {
            // Počkám, než doběhne compute thread
            inputCvar.wait(lock);
        };
        if(requestedHelp) {
            outputCvar.notify_one();
        }
    }
}

void Game::computeThread() {
    this_thread::sleep_for(chrono::milliseconds(100));
    outputCvar.notify_one();

    while(!quitRequested) {
        // Vyskytnul se nějaký error, který je potřeba zobrazit v konzoli
        if (errorMessage != "") {
            inputCvar.notify_one();
            outputCvar.notify_one();
        }
        // Pokud nejsem v pvp modu, tak hraje počítač
        // V případě rozšiřování programu o další módy (např. více náročné AI protihráče), tak zde je mozne vlozit funkci pro vypocitani a provedeni tahu.
        if (currentGameMode != Game::PlayerVsPlayer && (!player1Playing && squares_left != 0)) {
            makeComputerSimpleMove();
        }
        // Hráč provedl pohyb, evaluace zadaného pohybu
        if (playerInput.first != -1) {
            if(markSpotAndEvaluateWin()) {
                if (squares_left == 0) {
                    quitRequested = true;
                }
                inputCvar.notify_one();
                outputCvar.notify_one();
            } else {
                errorMessage = "Error: this line is already marked. Choose a different one.";
            }
        }
        // Každých 100ms kontroluji, jestli se něco nového nestalo
        this_thread::sleep_for(chrono::milliseconds(100));
    }
    inputCvar.notify_one();
    outputCvar.notify_one();
}

void Game::outputThread() {
    unique_lock<mutex> lock(outputMutex);

    while(!quitRequested) {
        outputCvar.wait(lock);
        if (pointScored) {
            // Pokud nějaký hráč získal v tahu bod, tak to vždycky vytiskne hrací plochu
            printboard();
        } else {
            // Pokud jsem v PlayerVsPlayer modu, tak vždycky vytisknu plochu (i pro reálého hráče 2)
            if (currentGameMode != Game::PlayerVsPlayer) {
                // Pokud právě odehrál počítač, tak netisknu hrací plochu
                if (player1Playing) {
                    printboard();
                }
            } else {
                printboard();
            }
        }
    }
    if (currentGameMode != Game::PlayerVsPlayer) {
        if (!player1Playing) {
            printboard();
        }
    }

    // Vyhodnoceni hry, vypis vysledku
    cout << endl << "-------- Game has ended -------" << endl;
    if (squares_left != 0) {
        cout << "Game was quit on the \"q\" signal" << endl;
    } else {
        if (score.first == score.second) {
            cout << "Game ended with a tie. Congratulations to both of you and thanks for playing!" << endl;
        } else if (score.first > score.second && currentGameMode == Game::PlayerVsPlayer) {
            // Player 1 won over Player 2
            cout << "Player 1 won with " << score.first << " point(s), whether as Player 2 got only " << score.second << " point(s)." << endl;
        } else if (score.first > score.second && currentGameMode == Game::PlayerVsSimpleComputer) {
            // Computer won
            cout << "Player 1 won with " << score.first << " point(s), whether as Computer got only " << score.second << " point(s)." << endl;
        } else if (score.first < score.second && currentGameMode == Game::PlayerVsPlayer) {
            // Player 2 won over Player one
            cout << "Player 2 won with " << score.second << " point(s), whether as Player 1 got only " << score.first << " point(s)." << endl;
        } else if (score.first < score.second && currentGameMode == Game::PlayerVsSimpleComputer) {
            // Computer lost
            cout << "Computer won with " << score.second << " point(s), whether as you got only " << score.first << " point(s)." << endl;
        }
        cout << "Thank you for playing and see you at the rematch!" << endl;
    }
}

bool Game::getUserInput() {
    // Nechci brát input pokud nejsem v modu PlayerVsPlayer, tj. hraji proti pocitaci, tak nechci prijimat input.
    if (currentGameMode != Game::PlayerVsPlayer && !player1Playing) {
        return false;
    }

    // Vyzva ke hre jednoho z hracu
    if (player1Playing) {
        cout << "Player 1, it is your turn. Example entry is 0 1 -u. Please type your move here: ";
    } else {
        cout << "Player 2, it is your turn. Example entry is 0 1 -u. Please type your move here: ";
    }

    // Cteni vstupu
    string line;
    getline(cin, line);
    stringstream stream(line);

    // Validace, zda nechci ukoncit hru
    if (line[0] == 'q') {
        //cout << "quit requested" << endl;
        quitRequested = true;
        return true;
    }

    // Tisknuti instrukci, pokud o to uzivatel pozadal
    if (line == "--help") {
        requestedHelp = true;
        return false;
    }

    // Validace inputu, pripadne tisteni chybovych hlasek
    size_t x_rows_input, y_cols_input;
    std::string option;
    if (stream >> x_rows_input >> y_cols_input >> option) {
        if (stream.rdbuf()->in_avail() == 0) {
            if (0 <= x_rows_input && x_rows_input < boardRows && 0 <= y_cols_input && y_cols_input < boardCols) {
                if(option == "-l" || option == "-r" || option == "-u" || option == "-d") {
                    // ---- Input je validni, nasledne bude jeste zkontrolovan ve funkci markSpotAndEvaluateWin(), zda toto misto jiz neni oznacene  ----
                    // Probiha konverze na souradnice relativni k board.
                    playerInput = convert_box_to_coord(x_rows_input, y_cols_input, option);
                    return true;
                } else {
                    errorMessage = "Error: invalid side identificator entered. Use -l for left, -r for right, -u for up or -d for down. Try again.";
                }
            } else {
                errorMessage = "Error: coordinates out of bounds. Try again";
            }
        } else {
            errorMessage = "Error: wrong input format entered. Try again";
        }
    } else {
        errorMessage = "Error: wrong input format entered. Try again";
    }
    return true;
}

void Game::printboard() {
    // Pokud jsem zacal hru, tak chci vytisknout instrukce a nedavat ANSI_CLEAR. Ve vsech ostatnich pripadech davam ANSI_CLEAR.
    if (started) {
        cout << ANSI_CLEAR;
    } else {
        started = true;
    }

    // Tisknu instrukce, pokud jsem o to zadal na vstupu.
    if(requestedHelp) {
        printHelp();
        requestedHelp = false;
    }

    // Prints error message if it has been filled somewhere in the code.
    if (errorMessage != "") {
        cout << COLOR_RED;
        cout << errorMessage << endl;
        cout << ANSI_COLOR_RESET;
        errorMessage = "";
    }

    // Prints cheering message if a player scored a point
    if (pointScored) {
        cout << COLOR_GREEN;
        if (player1Playing) {
            cout << "Good job Player 1, you scored a point! You can play one more time now." << endl;
        } else {
            // Prints only when in player vs player mode, when player 2 is not a computer.
            if (currentGameMode == Game::PlayerVsPlayer) {
                cout << "Good job Player 2, you scored a point! You can play one more time now." << endl;
            }
        }
        cout << ANSI_COLOR_RESET;
        pointScored = false;
    }

    cout << "To show help enter \"--help\" and to quit game enter \"q\"." << endl;

    cout << "------------------------------------" << endl;

    // ---------- PRINTING OF THE BOARD ----------
    bool je_lichy = true;
    // y_axis je string, ktery obsahuje vizualizaci y souradnice. Jakmile je vytvoren, tak je vytisten do konzole.
    string y_axis = "     ";
    // ---- print y axis ----
    for(size_t col = 0; col < boardCols - 1; ++col) {
        // pokud je souradnice dvojciferna, tak to ubere mezeru, aby se tam ta cisla vesla
        if (col >= 10) {
            y_axis += to_string(col) + "   ";
        } else {
            y_axis += to_string(col) + "    ";
        }

    }
    cout << y_axis << "y" << endl;

    // ---- tisteni mrizky ----
    for(size_t row = 0; row < boardRows; ++row) {
        // ---- Tisknuti x souradnic na zacatku radku ----
        // tisknuti cisel radku (na zacatku kazdeho sudeho radku)
        if (je_lichy) {
            cout << "   \u22C5";
        } else {
            // Pokud je cislo radku jednociferne, tak k nemu pridavam mezeru, abych tim kompenzoval velikost radku
            int x_marker = ((row+1)/2-1);
            if (x_marker < 10) {
                cout << to_string(((row+1)/2-1)) + " ";
            } else {
                cout << to_string((row+1)/2-1);
            }

        }
        // ----- Tisteni radku -----
        for (size_t col = 0; col < board[row].size(); ++col) {
            if (je_lichy) {
                // tisknuti hornich/dolnich stran
                if (board[row][col] == 0) {
                    // strana neni oznacena
                    cout << " -- \u22C5";
                } else if (board[row][col] == 1) {
                    // strana je oznacena
                    cout << "(--)\u22C5";
                }
            } else {
                // tisknuti levych/pravych stran
                if (board[row][col] == 0) {
                    // strana neni oznacena
                    cout << " | ";
                } else if (board[row][col] == 1) {
                    // strana je oznacena
                    cout << "(|)";
                }
                // ----- tisteni vybarveneho policka, pokud se nachazi ve wonSpots -----
                if (col < boardCols - 1 && wonSpots[(row + 1) / 2 - 1][col] == 1) {
                    cout << BG_COLOR_RED;
                    cout << "  ";
                    cout << BG_COLOR_DEFAULT;
                } else if (col < boardCols - 1 && wonSpots[(row + 1) / 2 - 1][col] == 2) {
                    cout << BG_COLOR_GREEN;
                    cout << "  ";
                    cout << BG_COLOR_DEFAULT;
                } else {
                    cout << "  ";
                }
            }
        }
        je_lichy = !je_lichy;
        cout << endl;
    }
    cout << "x" << endl;
    cout << "---------- Current score : ----------" << endl;
    cout << "Player 1 (red): " << score.first << " point(s)" << endl;
    if (currentGameMode == Game::PlayerVsPlayer) {
        cout << "Player 2 (green): " << score.second << " point(s)" << endl;
    } else {
        cout << "Computer (green): " << score.second << " point(s)" << endl;
    }

    cout << "------------------------------------" << endl;
}

pair<int, int> Game::convert_box_to_coord(size_t row, size_t col, std::string option) {
    size_t row_converted = 2 * row + 1;
    if (option == "-u") {
        row_converted -= 1;
    } else if (option == "-d") {
        row_converted += 1;
    }
    size_t col_converted = col;
    if (option == "-r") {
        col_converted += 1;
    }
    return {row_converted, col_converted};
}

bool Game::markSpotAndEvaluateWin() {
    if(board[playerInput.first][playerInput.second] == 1) {
        // Pole je obsazene, vracim false
        playerInput = {-1, -1};
        return false;
    } else {
        board[playerInput.first][playerInput.second] = 1;
        std::vector<pair<int, int>> neighbouring_rects;
        // ---- Find 1 or 2 squares bordering with the selected side ----
        // najdu si souřadnice těch 2 čtverců, ktere hranici s moji stranou, a pak na ně projedu postupně parametry -u, -d, -l, -r (cimz overim, zda jsou cele ohranicene)
        if (playerInput.first % 2 == 0) {
            // Jsem na sudém řádku, tj. na -u a -d
            if (playerInput.first == 0) {
                // nemám horní sousední čtverec (oznacena strana je uplne nahore)
                neighbouring_rects.push_back({0, playerInput.second});
            } else if (playerInput.first == boardRows - 1) {
                // nemám dolní sousední čtverec (oznacena strana je uplne dole)
                neighbouring_rects.push_back({playerInput.first / 2 - 1, playerInput.second});
            } else {
                // mam oba sousedni ctverce
                neighbouring_rects.push_back({playerInput.first / 2 - 1, playerInput.second});
                neighbouring_rects.push_back({playerInput.first / 2, playerInput.second});
            }
        } else {
            // Jsem na lichém řádku, tj. na -l a -r
            if (playerInput.second == 0) {
                // nemám levý sousední čtverec (oznacena strana je uplne vlevo)
                neighbouring_rects.push_back({(playerInput.first - 1) / 2, playerInput.second});
            } else if (playerInput.second == boardCols - 1) {
                // nemám pravý sousední čtverec (oznacena strana je uplne vpravo)
                neighbouring_rects.push_back({(playerInput.first - 1) / 2, playerInput.second - 1});
            } else {
                // mam oba sousedni ctverce
                neighbouring_rects.push_back({(playerInput.first - 1) / 2, playerInput.second});
                neighbouring_rects.push_back({(playerInput.first - 1) / 2, playerInput.second - 1});
            }
        }

        // Nasel jsem ctverec/ctverce, ktere sousedi s moji nove oznacenou stranou. Ted se podivam, jestli jsem tim tahem neziskal bod, tj. jestli jsem tim neuzavrel hranice ctverce.
        std::array<std::string, 4> options;
        options[0] = "-u";
        options[1] = "-d";
        options[2] = "-l";
        options[3] = "-r";
        for (const auto& rect : neighbouring_rects) {
            // is_full určuje, zda byl uzavřen čtverec (tj. jsou kolem něho nakreslené 4 čáry)
            bool is_full = true;
            for (const auto& option : options) {
                pair<int, int> side = convert_box_to_coord(rect.first, rect.second, option);
                if (board[side.first][side.second] == 0) {
                    is_full = false;
                    break;
                }
            }
            // Pokud byl čtverec uzavřen, hráč získává bod
            if (is_full) {
                pointScored = true;
                squares_left -= 1;
                if(player1Playing) {
                    score.first += 1;
                    wonSpots[rect.first][rect.second] = 1;
                } else {
                    score.second += 1;
                    wonSpots[rect.first][rect.second] = 2;
                }
            }
        }
        //  Pokud jsem scoroval bod, tak hrac hraje jeste jednou (tj. nestridaji se)
        if (!pointScored) {
            player1Playing = !player1Playing;
        }
    }
    // Pokud nejsem v modu PlayerVsPlayer, tj. hraji proti pocitaci, tak odstranuji pohyb z freeSpaces.
    if (currentGameMode != Game::PlayerVsPlayer) {
        size_t moveToErase = findElementIndexInPair(make_pair(playerInput.first, playerInput.second));
        if (moveToErase != -1) {
            freeSpaces.erase(freeSpaces.begin() + moveToErase);
        }
    }
    // konec pohybu, nastaveni playerInput na defaultni hodnotu
    playerInput = {-1, -1};
    return true;
}

void Game::printHelp() {
    cout << "--------------------- INSTRUCTIONS --------------------------------" << endl;
    cout << "Goal of this game is to mark lines around as many sqares as possible." << endl;
    cout << "You can create a line by first specyfing coordinates of its boardering square and second sepcifying the sqare's side." << endl;
    cout << "You can specify the side by entering -l for left, -r for right, -u for up or -d for down. Example entry can be \"0 1 -u\"." << endl;
    cout << "That means there can be up to 2 ways of marking one specific line." << endl;
    cout << "You recieve a point when you make the 4th line around a square in your move, therefore closing it in it's boundaries" << endl;
    cout << "The player with the bigger amounth of points wins the game." << endl;
}

size_t Game::findElementIndexInPair(const pair<int, int>& spotToSearch) {
    for (size_t i = 0; i < freeSpaces.size(); ++i) {
        if (freeSpaces[i] == spotToSearch) {
            return i;  // Nalezeno, vrátí index
        }
    }
    return -1;  // Nenalezeno, vrátí -1
}

void Game::makeComputerSimpleMove() {
    if (freeSpaces.size() > 0) {
        // Generating a random move
        std::random_device rd;
        std::mt19937 generator(rd());

        std::uniform_int_distribution<int> distribution(0, freeSpaces.size() - 1);
        int randomMove = distribution(generator);

        // Setting a new-found move to the playerInput
        playerInput = {freeSpaces[randomMove].first, freeSpaces[randomMove].second};
    }
}