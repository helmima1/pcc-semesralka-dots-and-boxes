#ifndef PCC_SEMESRALKA_DOTS_AND_BOXES_GAME_H
#define PCC_SEMESRALKA_DOTS_AND_BOXES_GAME_H

#include <vector>
#include <condition_variable>
#include <mutex>

using namespace std;

class Game {
public:
    /*
     * GameMode is used to differ which game mode should the game be played in.
     * Currently, there are implemented 2 game modes, one for player vs player and player vs computer.
     */
    enum GameMode {
        PlayerVsPlayer,
        PlayerVsSimpleComputer
    };

    /*
     * Constructors
     */
    Game();
    Game(size_t rows, size_t cols, GameMode gameMode);
    Game(GameMode gameMode);
    ~Game() = default;

    /*
     * True if player pressed "q" key or all lines in the game has been marked.
     * False if otherwise.
     */
    volatile bool quitRequested = false;

    /*
     * Starts the game by starting all threads.
     */
    void start();
private:
    /*
     * Current mode of the game.
     */
    GameMode currentGameMode;

    /*
     * How many rows and columns does the playing field have. These are coordinates relative to squares.
     */
    size_t xRows;
    size_t yCols;

    /*
     * How many rows and columns does the playing field have. These are coordinates relative to lines.
     */
    size_t boardRows;
    size_t boardCols;

    /*
     * Playing board of the game. Elements (lines) can have values 0 or 1, 0 = not marked, 1 = marked.
     */
    vector<vector<int>> board;

    /*
     * List of free spaces for the player vs computer game mode. They are in coordinates relative to the board.
     * If currentGameMode is set to PlayerVsPlayer, this stays uninitialized.
     */
    vector<pair<int, int>> freeSpaces;

     /*
      * Marks spots that has been won by either player.
      * 3 states: 0 = not taken, 1 = taken by player 1, 2 = taken by player 2
      */
    vector<vector<int>> wonSpots;

    /*
     * How many squares are left till all are marked. If it equals 0, then computeThread ends its activity.
     */
    size_t squares_left;

    /*
     * Input for the next move. It can be either set by a player in getInput function, or it can be set by computer, when
     * it makes its move in PlayerVsSimpleComputer game mode.
     */
    pair<int, int> playerInput = {-1, -1};

    /*
     * Determines which player is playing. When true, Player 1 is playing. When false, either Player 2 or computer plays,
     * depending on selected currentGameMode.
     */
    bool player1Playing = true;

    /*
     * Current score, score.first is for Player 1 and score.second is either for Player 2 or Computer depending on selected currentGameMode.
     */
    pair<int, int> score = {0, 0};

    /*
     * Determines if the game has been already started or not. It is used only in function printBoard() to not clear the
     * terminal while instructions at the start of the game are displayed.
     */
    bool started = false;

    /*
     * Tells outputThread, if it should print help (requested by a player at the input).
     */
    bool requestedHelp = false;

    /*
     * Stores error message if it appeared somewhere during the run.
     */
    string errorMessage = "";

    /*
     * Determines if points have been scored in last move, therefore a player has one more move allowed.
     */
    bool pointScored = false;

    condition_variable inputCvar, outputCvar;
    mutex inputMutex, outputMutex;

    /*
     * Prints current state of the board including cheering points scored message or error message.
     */
    void printboard();

    /*
     * Thread definitions
     */
    void inputThread();
    void outputThread();
    void computeThread();

    /*
     * Converts coordinates relative to squares (as taken by userInput) to coordinates relative to board.
     */
    pair<int, int> convert_box_to_coord(size_t row, size_t col, string option);

    /*
     * Marks a side chosen by the player. If the newly marked side closes a square, it adds a point to the corresponding player.
     * Returns true if the spot can be marked and false, if it cannot be marked (it has been marked before).
     */
    bool markSpotAndEvaluateWin();

    /*
     * Prints instructions/help to the console.
     */
    void printHelp();

    /*
     * Get user input from the console and validates it.
     * Returns false if q key was pressed, otherwise returns true.
     */
    bool getUserInput();

    /*
     * Chooses a random move on the board simulating a move made by a Computer player. It is done by looking at the freeSpaces and selecting a random index.
     * After that it saves this in the playerInput variable so that compute thread evaluates it as valid move.
     */
    void makeComputerSimpleMove();

    /*
     * Determines if wanted coordinates are in freeSpots. If yes, it returns its index in the std::vector freeSpots.
     * If not, it returns -1.
     */
    size_t findElementIndexInPair(const pair<int, int>& spotToSearch);

    /*
     * Creates playing board, wonSpots and freeSpots. Initializes boardRows, boardCols and squares left.
     * This function is called once in the constructor at the start of every game.
     */
    void createPlayingBoard();
};


#endif //PCC_SEMESRALKA_DOTS_AND_BOXES_GAME_H
