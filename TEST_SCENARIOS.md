# Testování programu
Testování bylo vyzkoušeno ručně pomocí uvedených vstupů.

## Vložení argumentů
Testování vkládání argumentů při spouštění programu.
### Bez argumentů
```
./pcc_semesralka_dots_and_boxes 
```

### Argumenty pro pole 4x5 singleplayer
```
./pcc_semesralka_dots_and_boxes -rows 4 -cols -5 -pvc
```

### Argumenty pro pole 4x5 multiplayer
```
./pcc_semesralka_dots_and_boxes -rows 4 -cols -5 -pvp
```

### Nevalidní argumenty
```
./pcc_semesralka_dots_and_boxes -rows 4 
./pcc_semesralka_dots_and_boxes -cols 5
./pcc_semesralka_dots_and_boxes -rows 4 -cols 5 -lcc
./pcc_semesralka_dots_and_boxes -rows 4 -cols 5
./pcc_semesralka_dots_and_boxes -rows 0 -cols 0 -pvp
./pcc_semesralka_dots_and_boxes -rows 51 -cols 51 -pvp
./pcc_semesralka_dots_and_boxes faebkalw
```

## Ukončení programu
```
q
```
```
0 1 -u
q
```
Ukončení funguje bez problémů, minimálně pro ukázané vstupy.

## Zobrazení nápovědy

```
--help
0 0 -u
--help
q
```
Nápověda byla zobrazena správným způsobem.

## Tvorba hracího pole pomocí argumentů

### Hrací pole 1 x 1
```
./pcc_semesralka_dots_and_boxes -rows 1 -cols -1 -pvp
```

### Hrací pole 1 x 5
```
./pcc_semesralka_dots_and_boxes -rows 1 -cols 5 -pvp
```

### Hrací pole 5 x 1
```
./pcc_semesralka_dots_and_boxes -rows 5 -cols 1 -pvp
```

### Malé hrací pole 2 x 3
```
./pcc_semesralka_dots_and_boxes -rows 2 -cols 3 -pvp
```

### Velké hrací pole
```
./pcc_semesralka_dots_and_boxes -rows 20 -cols 18 -pvp
./pcc_semesralka_dots_and_boxes -rows 50 -cols 50 -pvp
```

## Player vs Player mód

### Testovací scénář pro pole o rozměrech hracího 2x3, získání 1 bodu:
```
1 1 -u
1 1 -r
1 1 -l
1 1 -d
q
```

### Testovací scénář pro pole o rozměrech hracího 2x3, získání 2 bodů najednou:
```
0 0 -u
0 1 -u
0 0 -d
0 1 -d
0 0 -l
0 1 -r
0 0 -r
q
```

### Testovací scénář pro pole o rozměrech hracího 2x3, výhra hráče 1 (varianta 1):
```
0 0 -u
0 0 -d
0 1 -u
0 1 -d
0 2 -u
0 2 -d
0 2 -r
0 2 -l
1 2 -l
0 1 -l
0 0 -l
1 2 -d
1 1 -d
1 2 -r
1 0 -d
1 0 -l
1 1 -l
```

### Testovací scénář pro pole o rozměrech hracího 2x3, výhra hráče 1 (varianta 2):
```
0 0 -u
0 0 -d
0 0 -r
0 0 -l
0 1 -r
0 1 -u
0 2 -r
0 1 -d
0 2 -d
1 2 -d
0 2 -u
1 2 -r
1 1 -d
1 0 -d
1 2 -l
1 0 -r
1 0 -l
```

### Testovací scénář pro pole o rozměrech hracího 2x3, výhra hráče 2 (varianta 1):
```
0 0 -u
0 0 -d
0 0 -r
0 0 -l
0 1 -r
0 1 -u
0 1 -d
0 2 -u
0 2 -r
0 2 -d
1 0 -d
1 0 -r
1 0 -l
1 1 -d
1 2 -d
1 2 -r
1 1 -r
```

### Testovací scénář pro pole o rozměrech hracího 2x3, výhra hráče 2 (varianta 2):
```
1 1 -u
1 1 -d
1 1 -r
1 1 -l
0 0 -d
0 0 -u
0 0 -r
0 1 -u
0 0 -l
1 0 -d
1 2 -d
1 0 -l
0 1 -r
1 2 -r
0 2 -d
0 2 -u
0 2 -r
```

### Testovací scénář pro pole o rozměrech hracího 2x3, remíza (varianta 1):
```
0 0 -u
0 0 -d
0 0 -r
1 0 -r
1 0 -d
1 1 -d
1 2 -d
1 2 -l
1 2 -r
0 0 -l
1 0 -l
0 2 -u
0 2 -l
0 2 -d
0 1 -u
0 2 -r
1 1 -u
```

### Testovací scénář pro pole o rozměrech hracího 2x3, remíza (varianta 2):
```
1 2 -u
1 2 -d
1 2 -r
1 2 -l
0 0 -d
0 0 -u
0 0 -r
1 0 -d
0 0 -l
0 1 -u
1 0 -l
0 1 -d
1 0 -r
1 1 -d
0 1 -r
0 2 -u
0 2 -r
```

### Testování nevalidních vstupů
Tento scénář vykreslil pouze horní stranu čtverce o souřadnicích 0 0, všechny ostatní vstupy byli vyhodnoceny jako nevalidní a uživatel o tom byl informován
```
fbivuwi
-1 -1 -u
30 30 -u
0 30 -u
30 0 -u
0 0 
0 0 -e
0 0 -u
0 0 -u
q
```

## Player vs Computer mód
Vzhledem k tomu, že je generování tahů počítačem náhodné, tak jsou jako součást testovacích scénářů uvedené i tahy, které vygeneroval počítač (označeno symbolem komentáře "//" ). Tahy počítače jsou uvedeny v souřadnicích stran (tj. jak jsou uložené v proměnné `board`).
### Testovací scénář pro pole o rozměrech hracího 2x3, Výhra hráče v posledním tahu:
```
0 0 -u
// Computer move: 3 2
0 0 -d
// Computer move: 1 2
0 0 -l
// Computer move: 3 3
0 0 -r
1 0 -d
// Computer move: 0 1
0 1 -d
1 1 -d
// Computer move: 3 1
// Computer move: 1 3
0 2 -u
// Computer move: 4 2
0 2 -d
1 0 -l
```

### Testovací scénář pro pole o rozměrech hracího 2x3, Výhra počítače v posledním tahu:
```
1 1 -u
// Computer move: 1 0
1 1 -d
// Computer move: 4 2
0 1 -u
// Computer move: 1 2
0 1 -l
1 0 -d
// Computer move: 3 0
1 2 -u
// Computer move: 3 1
0 0 -u
// Computer move: 1 3
1 2 -r
// Computer move: 2 0
// Computer move: 3 2
// Computer move: 0 2
```

### Testovací scénář pro pole o rozměrech hracího 2x3, remíza:
```
0 0 -u
// Computer move: 3 2
0 0 -d
// Computer move: 1 2
0 1 -d
// Computer move: 4 0
0 0 -r
// Computer move: 2 2
0 1 -u
0 0 -l
1 1 -d
// Computer move: 3 0
1 2 -d
// Computer move: 3 1
// Computer move: 3 3
// Computer move: 0 2
0 2 -r
```

### Testovací scénář pro pole o rozměrech hracího 2x3, výhra počítače, poslední tah byl proveden hráčem:
```
0 0 -r
// Computer move: 4 0
1 2 -u
// Computer move: 0 0
1 2 -d
// Computer move: 2 0
1 2 -r
// Computer move: 3 1
1 2 -l
1 1 -d
// Computer move: 3 0
// Computer move: 0 1
0 1 -r
// Computer move: 1 0
// Computer move: 2 1
// Computer move: 0 2
0 2 -r
```

### Testovací scénář pro pole o rozměrech hracího 2x3, výhra hráče, poslední tah byl proveden počítačem:
```
1 1 -u
// Computer move: 1 3
0 0 -u
// Computer move: 1 0
0 0 -d
// Computer move: 4 2
0 0 -r
1 0 -d
// Computer move: 3 1
1 0 -l
1 1 -d
// Computer move: 3 3
1 1 -r
1 2 -u
0 1 -r
// Computer move: 0 1
// Computer move: 0 2
```